import Pair.Pair;

import java.util.Vector;

/**
 * Created by felix on 22/02/17.
 */
public class MultiplyThreadsBasics {
    private Matrix  _a;
    private Matrix  _b;
    private Matrix  _result;

    MultiplyThreadsBasics(Matrix a, Matrix b) {
        _a = a;
        _b = b;
        _result = null;

    }

    private void initResult() {
        _result = new Matrix();
        _result.setSize(_a.GetSize().getFirst(), _b.GetSize().getSec(), 0);
    }
    public void run() {
        initResult();
        int cores = Runtime.getRuntime().availableProcessors();
            System.out.println("Cores = " + cores);
        Vector<Thread>  arr = new Vector<Thread>();
        for (int i = 0; i < cores; i++) {
            arr.add(new BasicsThreadCompute(_a, _b, _result, i, cores));
        }
        for (int i = 0; i < cores; i++) {
            arr.get(i).start();
        }
        for (int i = 0; i < cores; i++) {
            try {
                arr.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        _result.Display();
    }
/*
    public static void main(String arg[]) {
        ParseFile First = new ParseFile("MatrixFiles/32-1.txt");
        ParseFile Sec = new ParseFile("MatrixFiles/32-2.txt");
        First.GeneratMatrix();
        Sec.GeneratMatrix();

        Matrix a = First.getMatrix();
        Matrix b = Sec.getMatrix();
        if (a.canBeMultiplyWith(b) == true) {
            MultiplyThreadsBasics n = new MultiplyThreadsBasics(a, b);
            n.run();
        } else {
            System.out.println("No Multiplication");
        }
    }
    */
}
