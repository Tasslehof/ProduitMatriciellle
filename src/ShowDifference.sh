#!/usr/bin/sh

function execute() {
    echo "Matrix size: [" $1 "][" $2 "] * [" $3 "][" $4 "] with " $7 " cores" 
    echo "Naive"
    time java Naive ../MatrixFiles/$5 ../MatrixFiles/$6 $7 > /dev/null || echo "Naive fail to multipliate matrix"
    echo "Strassen"
    time java Strassen ../MatrixFiles/$5 ../MatrixFiles/$6 $7 > /dev/null || echo "Strassen fail to multipliate matrix"
    
}

execute 2 2 2 2 "2-1.txt" "2-2.txt" 1
execute 2 2 2 2 "2-1.txt" "2-2.txt" 2
execute 2 2 2 2 "2-1.txt" "2-2.txt" 4
execute 2 2 2 2 "2-1.txt" "2-2.txt" 8
execute 4 4 4 4 "4-1.txt" "4-2.txt" 1
execute 4 4 4 4 "4-1.txt" "4-2.txt" 2
execute 4 4 4 4 "4-1.txt" "4-2.txt" 4
execute 4 4 4 4 "4-1.txt" "4-2.txt" 8
execute 8 8 8 8 "8-1.txt" "8-2.txt" 1
execute 8 8 8 8 "8-1.txt" "8-2.txt" 2
execute 8 8 8 8 "8-1.txt" "8-2.txt" 4
execute 8 8 8 8 "8-1.txt" "8-2.txt" 8
execute 16 16 16 16 "16-1.txt" "16-2.txt" 1
execute 16 16 16 16 "16-1.txt" "16-2.txt" 2
execute 16 16 16 16 "16-1.txt" "16-2.txt" 4
execute 16 16 16 16 "16-1.txt" "16-2.txt" 8
execute 32 32 32 32 "32-1.txt" "32-2.txt" 1
execute 32 32 32 32 "32-1.txt" "32-2.txt" 2
execute 32 32 32 32 "32-1.txt" "32-2.txt" 4
execute 32 32 32 32 "32-1.txt" "32-2.txt" 8
execute 64 64 64 64 "64-1.txt" "64-2.txt" 1
execute 64 64 64 64 "64-1.txt" "64-2.txt" 2
execute 64 64 64 64 "64-1.txt" "64-2.txt" 4
execute 64 64 64 64 "64-1.txt" "64-2.txt" 8
execute 128 128 128 128 "128-1.txt" "128-2.txt" 1
execute 128 128 128 128 "128-1.txt" "128-2.txt" 2
execute 128 128 128 128 "128-1.txt" "128-2.txt" 4
execute 128 128 128 128 "128-1.txt" "128-2.txt" 8
execute 256 256 256 256 "256-1.txt" "256-2.txt" 1
execute 256 256 256 256 "256-1.txt" "256-2.txt" 2
execute 256 256 256 256 "256-1.txt" "256-2.txt" 4
execute 256 256 256 256 "256-1.txt" "256-2.txt" 8
execute 512 512 512 512 "512-1.txt" "512-2.txt" 1
execute 512 512 512 512 "512-1.txt" "512-2.txt" 2
execute 512 512 512 512 "512-1.txt" "512-2.txt" 4
execute 512 512 512 512 "512-1.txt" "512-2.txt" 8
