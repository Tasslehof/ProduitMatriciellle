import Pair.Pair;
import java.util.Vector;

public class Matrix {
    private Vector<Vector<Integer>>     _matrix;
    private int _nbCase;

    public Matrix() {
        _matrix = new Vector<Vector<Integer>>();
        _nbCase = 0;
    }
    public Matrix(Vector<Vector<Integer>> arg) {
        _matrix = arg;
        _nbCase = 0;
    }
    public Integer  GetAt(int i, int j) {
        return _matrix.get(i).get(j);
    }
    public void  SetAt(int i, int j, Integer value) {
            _matrix.get(i).set(j, value);
    }
    boolean canBeMultiplyWith(Matrix b) {
        if (_matrix.size() != b.GetSize().getSec() ||
                AllLineHasSameSize() == false ||
                b.AllLineHasSameSize() == false) {
            return false;
        } else {
            return true;
        }
    }
    public Pair<Integer, Integer> GetSize() {
        return new Pair<Integer, Integer>(_matrix.size(), _matrix.get(0).size());
    }
    public int  GetColumnSize() {
        return _matrix.size();
    }
    public int GetLineSize() {
        return _matrix.get(0).size();
    }
    public boolean AllLineHasSameSize() {
        for (int i = 0; i < _matrix.size(); i++) {
            if (_matrix.get(i).size() != _matrix.get(0).size()) {
                return false;
            }
        }
        return true;
    }
    public void AddNewLine(int idx, Vector<Integer> line) {
        _matrix.add(idx, line);
    }
    public void Display() {
        for (int i = 0; i < _matrix.size(); i++) {
            String  disp = "";
            for (int j = 0; j < _matrix.get(i).size(); j++) {
                if (j != 0) {
                    disp += "\t";
                }
                disp += _matrix.get(i).get(j);
            }
            System.out.println(disp);
        }
    }
    public void setSize(int i, int j, Integer v) {
        _matrix.setSize(i);
        for (int a = 0; a < i; a++) {
            _matrix.set(a, new Vector<Integer>());
            _matrix.get(a).setSize(j);
            for (int b = 0; b < j; b++) {
                _matrix.get(a).set(b, v);
            }
        }
    }
    Vector<Matrix> subdivid() {
        Vector<Matrix> ret = new Vector<Matrix>(4);
        Pair<Integer, Integer> size = GetSize();
        for (int i = 0; i < 4; i++) {
            Matrix n = new Matrix();
            n.setSize(size.getFirst() / 2, size.getSec() / 2, 0);
            Pair<Integer, Integer>  start = new Pair<Integer, Integer>((i / 2) * size.getFirst() / 2,
                    (i % 2) * size.getSec() / 2);
            Pair<Integer, Integer> end = new Pair<Integer, Integer>((i / 2) * size.getFirst() / 2 + size.getFirst() / 2,
                    ((i % 2) * size.getSec() / 2 + size.getSec() / 2));
            for (int a = start.getFirst(); a < end.getFirst(); a++) {
                for (int b = start.getSec(); b < end.getSec(); b++) {
                    n.SetAt(a - start.getFirst(), b - start.getSec(), GetAt(a, b));
                }
            }
            ret.add(i, n);
        }
        return ret;
    }
}
