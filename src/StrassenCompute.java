import Pair.Pair;

import java.util.Vector;
import java.util.concurrent.Callable;

/**
 * Created by felix on 17/03/17.
 */
public class StrassenCompute implements Callable<Matrix> {
    Matrix _a = null;
    Matrix _b = null;
    Matrix _r = null;
    Pair<Integer, Integer>  _size;
    public StrassenCompute(Matrix a, Matrix b) {
        _a = a;
        _b = b;
        _r = new Matrix();
        _r.setSize(_a.GetSize().getFirst(), _b.GetSize().getSec(), 0);
        _size = new Pair<Integer, Integer>(_a.GetSize().getFirst(), _b.GetSize().getSec());
    }

    public Matrix Algorythm(Matrix a, Matrix b) {
        Matrix  p1 = null;
        Matrix  p2 = null;
        Matrix  p3 = null;
        Matrix  p4 = null;
        Matrix  p5 = null;
        Matrix  p6 = null;
        Matrix  p7 = null;
        Matrix c11 = null;
        Matrix c12 = null;
        Matrix c21 = null;
        Matrix c22 = null;
        if (a.GetSize().getFirst() < 32) {
            return Strassen.Mult(a, b);
        } else {
            Vector<Matrix> ar = a.subdivid();
            Vector<Matrix> br = b.subdivid();
            Matrix atmp = Strassen.Add(ar.get(0), ar.get(3));
            Matrix btmp = Strassen.Add(br.get(0), br.get(3));
            p1 = Algorythm(Strassen.Add(ar.get(0), ar.get(3)), Strassen.Add(br.get(0), br.get(3)));
            p2 = Algorythm(Strassen.Add(ar.get(2), ar.get(3)), br.get(0));
            p3 = Algorythm(ar.get(0), Strassen.sub(br.get(1), br.get(3)));
            p4 = Algorythm(ar.get(3), Strassen.sub(br.get(2), br.get(0)));
            p5 = Algorythm(Strassen.Add(ar.get(0), ar.get(1)), br.get(3));
            p6 = Algorythm(Strassen.sub(ar.get(2), ar.get(0)), Strassen.Add(br.get(0), br.get(1)));
            p7 = Algorythm(Strassen.sub(ar.get(1), ar.get(3)), Strassen.Add(br.get(2), br.get(3)));
            c11 = Strassen.Add(Strassen.sub(Strassen.Add(p1, p4), p5), p7);
            c12 = Strassen.Add(p3, p5);
            c21 = Strassen.Add(p2, p4);
            c22 = Strassen.Add(Strassen.Add(Strassen.sub(p1, p2), p3), p6);
            Matrix ret = new Matrix();

            ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
            int l = a.GetSize().getFirst() / 2;
            for (int i = 0; i < ret.GetSize().getFirst() / 2; i++) {
                for (int j = 0; j < ret.GetSize().getSec() / 2; j++) {
                    ret.SetAt(i, j, c11.GetAt(i, j));
                    ret.SetAt(i, j + l, c12.GetAt(i, j));
                    ret.SetAt(i + l, j, c21.GetAt(i, j));
                    ret.SetAt(i + l, j + l, c22.GetAt(i, j));
                }
            }
            return ret;
        }
    }
    @Override
    public Matrix call() throws Exception {
        Matrix a = _a;
        Matrix b = _b;
        Matrix  p1 = null;
        Matrix  p2 = null;
        Matrix  p3 = null;
        Matrix  p4 = null;
        Matrix  p5 = null;
        Matrix  p6 = null;
        Matrix  p7 = null;
        Matrix c11 = null;
        Matrix c12 = null;
        Matrix c21 = null;
        Matrix c22 = null;
        Vector<Matrix> ar = a.subdivid();
        Vector<Matrix> br = b.subdivid();
        Matrix atmp = Strassen.Add(ar.get(0), ar.get(3));
        Matrix btmp = Strassen.Add(br.get(0), br.get(3));
        p1 = Algorythm(Strassen.Add(ar.get(0), ar.get(3)), Strassen.Add(br.get(0), br.get(3)));
        p2 = Algorythm(Strassen.Add(ar.get(2), ar.get(3)), br.get(0));
        p3 = Algorythm(ar.get(0), Strassen.sub(br.get(1), br.get(3)));
        p4 = Algorythm(ar.get(3), Strassen.sub(br.get(2), br.get(0)));
        p5 = Algorythm(Strassen.Add(ar.get(0), ar.get(1)), br.get(3));
        p6 = Algorythm(Strassen.sub(ar.get(2), ar.get(0)), Strassen.Add(br.get(0), br.get(1)));
        p7 = Algorythm(Strassen.sub(ar.get(1), ar.get(3)), Strassen.Add(br.get(2), br.get(3)));
        c11 = Strassen.Add(Strassen.sub(Strassen.Add(p1, p4), p5), p7);
        c12 = Strassen.Add(p3, p5);
        c21 = Strassen.Add(p2, p4);
        c22 = Strassen.Add(Strassen.Add(Strassen.sub(p1, p2), p3), p6);
        Matrix ret = new Matrix();

        ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
        int l = a.GetSize().getFirst() / 2;
        for (int i = 0; i < ret.GetSize().getFirst() / 2; i++) {
            for (int j = 0; j < ret.GetSize().getSec() / 2; j++) {
               ret.SetAt(i, j, c11.GetAt(i, j));
               ret.SetAt(i, j + l, c12.GetAt(i, j));
               ret.SetAt(i + l, j, c21.GetAt(i, j));
               ret.SetAt(i + l, j + l, c22.GetAt(i, j));
            }
        }
        return ret;
    }
}
