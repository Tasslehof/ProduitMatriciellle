/**
 * Created by felix on 16/03/17.
 */
public class NaiveCompute implements Runnable {
    private Matrix _a = null;
    private Matrix _b = null;
    private Matrix _r = null;
    private int _i = 0;
    private int _j = 0;

    public NaiveCompute(Matrix a, Matrix b, Matrix r, int i, int j) {
        _a = a;
        _b = b;
        _r = r;
        _i = i;
        _j = j;
    }
    public void run() {
        _r.SetAt(_i, _j, compute(_i, _j));
    }
    public Integer compute(int a, int b) {
        Integer ret = 0;
        for (int p = 0; p < _a.GetSize().getSec(); p++) {
            ret += _a.GetAt(a, p) * _b.GetAt(p, b);
        }
        return ret;
    }
}
