package Pair;

public class Pair<T, U> {
    private T   _first;
    private U   _sec;

    public Pair(T first, U sec) {
        _first = first;
        _sec = sec;
    }
    public T getFirst() {
        return _first;
    }
    public U getSec() {
        return _sec;
    }
    public void setFirst(T first) {
        _first = first;
    }
    public void secSec(U sec) {
        _sec = sec;
    }
}
