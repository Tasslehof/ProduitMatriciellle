
import Pair.Pair;

import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.lang.System.exit;

public class Strassen {

    Matrix _a = null;
    Matrix _b = null;
    Matrix _f = null;
    Matrix _s = null;
    Matrix _r = null;
    int     _core = 1;
    Pair<Integer, Integer> _size = null;

    static int powerOfTwo(int a) {
        int ret = 1;
        while (ret < a) {
            ret *= 2;
        }
        return ret;
    }
    static Matrix Add(Matrix a, Matrix b) {
        Matrix ret = new Matrix();
        ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
        for (int i = 0; i < a.GetSize().getFirst(); i++) {
            for (int j = 0; j < a.GetSize().getSec(); j++) {
                ret.SetAt(i, j, a.GetAt(i, j) + b.GetAt(i, j));
            }
        }
        return ret;
    }
    static Matrix sub(Matrix a, Matrix b) {
        Matrix ret = new Matrix();
        ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
        for (int i = 0; i < a.GetSize().getFirst(); i++) {
            for (int j = 0; j < a.GetSize().getSec(); j++) {
                ret.SetAt(i, j, a.GetAt(i, j) - b.GetAt(i, j));
            }
        }
        return ret;
    }
    static Matrix Mult(Matrix a, Matrix b) {
        Matrix ret = new Matrix();
        ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
        Pair<Integer, Integer> size = ret.GetSize();
        for (int i = 0; i < a.GetSize().getFirst(); i++) {
            for (int j = 0;j < b.GetSize().getSec(); j++) {
                int sum = 0;
                for (int p = 0; p < a.GetSize().getSec(); p++) {
                    sum += a.GetAt(i, p) * b.GetAt(p, j);
                }
                ret.SetAt(i, j, sum);
            }
        }
        return ret;
    }
    public Strassen(Matrix a, Matrix b, int core) {
        _a = a;
        _b = b;
        _core = core;
        _r = new Matrix();
        _r.setSize(_a.GetSize().getFirst(), _b.GetSize().getSec(), 0);
        _size = new Pair<Integer, Integer>(_a.GetSize().getFirst(), _b.GetSize().getSec());
        InitPad();
    }
    static int getMax(Pair<Integer, Integer> p) {
        if (p.getFirst() > p.getSec()) {
            return p.getFirst();
        }
        return p.getSec();
    }
    public void InitPad() {
        _f = new Matrix();
        _s = new Matrix();
        int AMax = Strassen.getMax(_a.GetSize());
        int BMax = Strassen.getMax(_b.GetSize());
        int AP = Strassen.powerOfTwo(AMax);
        int BP = Strassen.powerOfTwo(BMax);
        _f.setSize(AP, AP, 0);
        _s.setSize(BP, BP, 0);

        Pair<Integer, Integer>  as = _a.GetSize();
        Pair<Integer, Integer>  bs = _b.GetSize();

        for (int i = 0; i < as.getFirst(); i++) {
            for (int j = 0; j < as.getSec(); j++) {
                _f.SetAt(i, j, _a.GetAt(i, j));
            }
        }
        for (int i = 0; i < bs.getFirst(); i++) {
            for (int j = 0; j < bs.getSec(); j++) {
                _s.SetAt(i, j, _b.GetAt(i, j));
            }
        }
    }
    public Matrix AlgorythmThread(Matrix a, Matrix b) throws Exception {
        ExecutorService pool = Executors.newFixedThreadPool(_core);
        Matrix  p1 = null;
        Matrix  p2 = null;
        Matrix  p3 = null;
        Matrix  p4 = null;
        Matrix  p5 = null;
        Matrix  p6 = null;
        Matrix  p7 = null;
        Matrix c11 = null;
        Matrix c12 = null;
        Matrix c21 = null;
        Matrix c22 = null;
        if (a.GetSize().getFirst() < 4) {
            return Strassen.Mult(a, b);
        } else {
            Vector<Matrix> ar = a.subdivid();
            Vector<Matrix> br = b.subdivid();
            Matrix atmp = Strassen.Add(ar.get(0), ar.get(3));
            Matrix btmp = Strassen.Add(br.get(0), br.get(3));
            Callable<Matrix> cp1 = new StrassenCompute(Strassen.Add(ar.get(0), ar.get(3)), Strassen.Add(br.get(0), br.get(3)));
            Callable<Matrix> cp2 = new StrassenCompute(Strassen.Add(ar.get(2), ar.get(3)), br.get(0));
            Callable<Matrix> cp3 = new StrassenCompute(ar.get(0), Strassen.sub(br.get(1), br.get(3)));
            Callable<Matrix> cp4 = new StrassenCompute(ar.get(3), Strassen.sub(br.get(2), br.get(0)));
            Callable<Matrix> cp5 = new StrassenCompute(Strassen.Add(ar.get(0), ar.get(1)), br.get(3));
            Callable<Matrix> cp6 = new StrassenCompute(Strassen.sub(ar.get(2), ar.get(0)), Strassen.Add(br.get(0), br.get(1)));
            Callable<Matrix> cp7 = new StrassenCompute(Strassen.sub(ar.get(1), ar.get(3)), Strassen.Add(br.get(2), br.get(3)));
            Future<Matrix>  fp1;
            Future<Matrix>  fp2;
            Future<Matrix>  fp3;
            Future<Matrix>  fp4;
            Future<Matrix>  fp5;
            Future<Matrix>  fp6;
            Future<Matrix>  fp7;

            fp1 = pool.submit(cp1);//.get();
            fp2 = pool.submit(cp2);//.get();
            fp3 = pool.submit(cp3);//.get();
            fp4 = pool.submit(cp4);//.get();
            fp5 = pool.submit(cp5);//.get();
            fp6 = pool.submit(cp6);//.get();
            fp7 = pool.submit(cp7);//.get();
            pool.shutdown();
            p1 = fp1.get();
            p2 = fp2.get();
            p3 = fp3.get();
            p4 = fp4.get();
            p5 = fp5.get();
            p6 = fp6.get();
            p7 = fp7.get();
            c11 = Strassen.Add(Strassen.sub(Strassen.Add(p1, p4), p5), p7);
            c12 = Strassen.Add(p3, p5);
            c21 = Strassen.Add(p2, p4);
            c22 = Strassen.Add(Strassen.Add(Strassen.sub(p1, p2), p3), p6);
            Matrix ret = new Matrix();

            ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
            int l = a.GetSize().getFirst() / 2;
            for (int i = 0; i < ret.GetSize().getFirst() / 2; i++) {
                for (int j = 0; j < ret.GetSize().getSec() / 2; j++) {
                    ret.SetAt(i, j, c11.GetAt(i, j));
                    ret.SetAt(i, j + l, c12.GetAt(i, j));
                    ret.SetAt(i + l, j, c21.GetAt(i, j));
                    ret.SetAt(i + l, j + l, c22.GetAt(i, j));
                }
            }
            return ret;
        }
    }
/*
    public Matrix Algorythm(Matrix a, Matrix b) {
        Matrix  p1 = null;
        Matrix  p2 = null;
        Matrix  p3 = null;
        Matrix  p4 = null;
        Matrix  p5 = null;
        Matrix  p6 = null;
        Matrix  p7 = null;
        Matrix c11 = null;
        Matrix c12 = null;
        Matrix c21 = null;
        Matrix c22 = null;
        if (a.GetSize().getFirst() < 4) {
            return Strassen.Mult(a, b);
        } else {
            Vector<Matrix> ar = a.subdivid();
            Vector<Matrix> br = b.subdivid();
            Matrix atmp = Strassen.Add(ar.get(0), ar.get(3));
            Matrix btmp = Strassen.Add(br.get(0), br.get(3));
            p1 = Algorythm(Strassen.Add(ar.get(0), ar.get(3)), Strassen.Add(br.get(0), br.get(3)));
            p2 = Algorythm(Strassen.Add(ar.get(2), ar.get(3)), br.get(0));
            p3 = Algorythm(ar.get(0), Strassen.sub(br.get(1), br.get(3)));
            p4 = Algorythm(ar.get(3), Strassen.sub(br.get(2), br.get(0)));
            p5 = Algorythm(Strassen.Add(ar.get(0), ar.get(1)), br.get(3));
            p6 = Algorythm(Strassen.sub(ar.get(2), ar.get(0)), Strassen.Add(br.get(0), br.get(1)));
            p7 = Algorythm(Strassen.sub(ar.get(1), ar.get(3)), Strassen.Add(br.get(2), br.get(3)));
            c11 = Strassen.Add(Strassen.sub(Strassen.Add(p1, p4), p5), p7);
            c12 = Strassen.Add(p3, p5);
            c21 = Strassen.Add(p2, p4);
            c22 = Strassen.Add(Strassen.Add(Strassen.sub(p1, p2), p3), p6);
            Matrix ret = new Matrix();

            ret.setSize(a.GetSize().getFirst(), a.GetSize().getSec(), 0);
            int l = a.GetSize().getFirst() / 2;
            for (int i = 0; i < ret.GetSize().getFirst() / 2; i++) {
                for (int j = 0; j < ret.GetSize().getSec() / 2; j++) {
                    ret.SetAt(i, j, c11.GetAt(i, j));
                    ret.SetAt(i, j + l, c12.GetAt(i, j));
                    ret.SetAt(i + l, j, c21.GetAt(i, j));
                    ret.SetAt(i + l, j + l, c22.GetAt(i, j));
                }
            }
            return ret;
        }
    }
    */
    public void Reduce() {
        Matrix  n = new Matrix();
        n.setSize(_a.GetSize().getFirst(), _b.GetSize().getSec(), 0);
        for (int i = 0; i < _a.GetSize().getFirst(); i++) {
            for (int j = 0; j < _a.GetSize().getSec(); j++) {
                n.SetAt(i, j, _r.GetAt(i, j));
            }
        }
        _r = n;
    }
    public void run() throws Exception {
        _r = AlgorythmThread(_f, _s);
        Reduce();
        _r.Display();
    }


    public static void main(String arg[]) throws Exception {
        if (arg.length < 2) {
            System.out.println("Need two Matrix file");
            exit(42);
        }
        try {
            ParseFile First = new ParseFile(arg[0]);
            //"MatrixFiles/1024-1.txt");
            ParseFile Sec = new ParseFile(arg[1]);
            //"MatrixFiles/1024-2.txt");

            First.GeneratMatrix();
            Sec.GeneratMatrix();
            Matrix a = First.getMatrix();
            Matrix b = Sec.getMatrix();
            if (a.canBeMultiplyWith(b) == true) {
                int nbcore = Runtime.getRuntime().availableProcessors();
                if (arg.length > 2) {
                    nbcore = Integer.parseInt(arg[2]);
                    if (nbcore <= 0) {
                        System.out.println("Can not have " + nbcore );
                        nbcore = Runtime.getRuntime().availableProcessors();
                        System.out.println("Run with " + nbcore + " cores");
                    }
                }
                Strassen n = new Strassen(a, b, nbcore);
                n.run();
            } else {
                System.out.println("No multiplication");
                exit(42);
            }
            exit(0);
        }
        catch (IOException e) {
            exit(42);
        }
    }
}
