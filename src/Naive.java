import Pair.Pair;
import org.omg.SendingContext.RunTime;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import static java.lang.System.exit;

public class Naive {
    private Matrix _f = null;
    private Matrix _s = null;
    private Matrix _r = null;
    private int _core;
    public Naive(Matrix a, Matrix b, int core) {
        _f = a;
        _s = b;
        _r = new Matrix();
        _r.setSize(_f.GetSize().getFirst(), _s.GetSize().getSec(), 0);
        _core = core;
    }
    public void run() {
        Pair<Integer, Integer> p = _r.GetSize();
        System.out.println("_core = " + _core);
        ExecutorService executor = Executors.newFixedThreadPool(_core);

        for (int i = 0; i < p.getFirst(); i++) {
            for (int j =0; j < p.getSec(); j++) {
                NaiveCompute c = new NaiveCompute(_f, _s, _r, i, j);
                executor.execute(c);
            }
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        _r.Display();
    }
    public static void main(String arg[]) {
        if (arg.length < 2) {
            System.out.println("Need two file with matrix");
            exit(42);
        }
        try {
            ParseFile First = new ParseFile(arg[0]);//"MatrixFiles/30-1.txt");
            ParseFile Sec = new ParseFile(arg[1]);//"MatrixFiles/30-2.txt");
            First.GeneratMatrix();
            Sec.GeneratMatrix();
            Matrix a = First.getMatrix();
            Matrix b = Sec.getMatrix();
            if (a.canBeMultiplyWith(b) == true) {
                int nbcore = Runtime.getRuntime().availableProcessors();
                if (arg.length > 2) {
                    nbcore = Integer.parseInt(arg[2]);
                    if (nbcore <= 0) {
                        System.out.println("Can not have " + nbcore );
                        nbcore = Runtime.getRuntime().availableProcessors();
                        System.out.println("Run with " + nbcore + " cores");
                    }
                }
                Naive n = new Naive(a, b, nbcore);
                n.run();
            } else {
                System.out.println("No multiplication");
                exit(42);
            }
            exit(0);
        } catch (IOException e) {
            exit(42);
        }
    }
}
