import Pair.Pair;

/**
 * Created by felix on 23/02/17.
 */
public class BasicsThreadCompute extends Thread {
    Matrix  _a;
    Matrix  _b;
    Matrix  _result;
    int     _idThread;
    int     _nbThread;

    public BasicsThreadCompute(Matrix a, Matrix b, Matrix r, int i, int cores) {
        _a = a;
        _b = b;
        _result = r;
        _idThread = i;
        _nbThread = cores;
    }

    public void run() {
        Pair<Integer, Integer> size = _result.GetSize();
        int nblinetoCompute = size.getFirst() / (_nbThread);
        int linebegincompute = (((size.getFirst() / _nbThread + 1) * _idThread)) % size.getFirst();
        int lineendcompute = (((size.getFirst() / _nbThread + 1 ) * (_idThread + 1))) % size.getFirst();
        if (lineendcompute < linebegincompute) {
            lineendcompute = size.getFirst();
        }

        int nb = 0;
        int m = _a.GetSize().getSec();
        for (int i = linebegincompute; i < lineendcompute; i++) {
            for (int p = 0; p < _a.GetSize().getSec(); p++) {
                int sum = 0;
                for (int j = 0; j < size.getSec(); j++) {
                    _result.SetAt(i, j, _result.GetAt(i, j) + _a.GetAt(i, p) * _b.GetAt(p, j));
                }
                nb++;
            }
            System.out.println("[" + _idThread + "] = " + (i - linebegincompute));
        }
        System.out.println("END : + " + _idThread);
        System.out.println("Thread[" + _idThread + "]\t[" + nblinetoCompute + "][" + linebegincompute + "][" + lineendcompute + "]");
    }
}
