import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import static java.lang.System.exit;

public class ParseFile {
    private String _fileName;
    private Matrix _matrix;

    public ParseFile(String filename) {
        _fileName = filename;
        _matrix = new Matrix();
    }
    public Matrix getMatrix() {
        return _matrix;
    }

    public Vector<Integer> ParseLineToVector(String line) {
        Vector<Integer> ret = new Vector<Integer>();
        String[] s = line.split(" ");
        int idx = 0;
        while (idx < s.length) {
            ret.add(Integer.valueOf(s[idx]));
            idx += 1;
        }
        idx = 0;
        while (idx < ret.size()) {
            idx += 1;
        }
        return ret;
    }

    public void GeneratMatrix() throws IOException {
        int idx = 0;
        try (FileReader reader = new FileReader(_fileName)) {
            try (BufferedReader br = new BufferedReader(reader)) {
                String line;
                while ((line = br.readLine()) != null) {
                    _matrix.AddNewLine(idx, ParseLineToVector(line));
                    idx += 1;
                }
            } catch (IOException e) {
                System.out.println("Invalid File : " + _fileName);
                throw e;
            }
        } catch (IOException e) {
            System.out.println("Invalid FileName : " + _fileName);
            throw e;
        }
    }
}
