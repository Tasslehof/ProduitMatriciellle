import Pair.Pair;
import java.util.Vector;

public class MultiplyWithoutThread {
    private Matrix                          _first;
    private Matrix                          _sec;
    private Vector<Vector<Integer>>         _result;

    public MultiplyWithoutThread(Matrix first, Matrix sec) {
        _first = first;
        _sec = sec;
        _result = null;
    }
    public boolean  CanMultiply() {
        Pair<Integer, Integer>  firstSize = _first.GetSize();
        Pair<Integer, Integer> secSize = _sec.GetSize();
        System.out.println("A [" + firstSize.getFirst() + "][" + firstSize.getSec() + "]");
        System.out.println("B [" + secSize.getFirst() + "][" + secSize.getSec() + "]");
        if (    firstSize.getSec() != secSize.getFirst() ||
                _first.AllLineHasSameSize() == false ||
                _sec.AllLineHasSameSize() == false) {
            return false;
        } else {
            return true;
        }
    }
    public void DisplayResult() {
        for (int i = 0; i < _result.size(); i++) {
            String disp = "";
            for (int j = 0; j < _result.get(i).size(); j++) {
                disp += " " + _result.get(i).get(j);
            }
            System.out.println(disp);
        }
    }
    public void initResult() {
        _result = new Vector<Vector<Integer>>();
        _result.setSize(_first.GetSize().getFirst());
        for (int i = 0; i < _first.GetSize().getFirst(); i++) {
            _result.set(i, new Vector<Integer>());
            _result.get(i).setSize(_sec.GetSize().getSec());
            for (int j = 0; j < _sec.GetSize().getSec(); j++) {
                _result.get(i).set(j, 0);
            }
        }
    }
    public void compute() {
        for (int i = 0; i < _result.size(); i++) {
            for (int j = 0; j < _result.get(i).size(); j++) {
                int sum = 0;
                for (int p = 0; p < _first.GetSize().getSec(); p++) {
                    sum += _first.GetAt(i, p) * _sec.GetAt(p, j);
                }
                System.out.println("[" + i + "][" + j + "]=" + sum);
                _result.get(i).set(j, sum);
            }
        }
        DisplayResult();
    }
}

