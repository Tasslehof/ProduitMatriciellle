# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>

# define FAILURE	-1
# define SUCCESS	0

int	checkIsNb(char const *const str) {
  if (str == NULL) {
    return FAILURE;
  }
  for (int i = 0; i < strlen(str); i++) {
    if (str[i] < '0' || str[i] > '9') {
      return FAILURE;
    }
  }
  return SUCCESS;
}

int	checkArg(char const *const f, char const * const s, char const *const t) {
  if (checkIsNb(f) == FAILURE) {
    dprintf(2, "%s is not a number", f);
    return FAILURE;
  }
  if (checkIsNb(s) == FAILURE) {
    dprintf(2, "%s is not a number", s);
    return FAILURE;
  }
  if (t != NULL && checkIsNb(t) == FAILURE) {
    dprintf(3, "%s is not a number", t);
    return FAILURE;
  }
  return SUCCESS;
}

int	GeneratMatrix(int x, int y, int max) {
  int	*tab = malloc(sizeof(int) * x);
  srand(time(NULL));

  if (tab == NULL) {
    dprintf(2, "Memory Error");
    return FAILURE;
  }
  for (int i = 0; i < y; i++) {
    for (int j = 0; j < x; j++) {
      tab[j] = rand() % max;
    }
    for (int k = 0; k < x; k++) {
      printf("%d ", tab[k]);
    }
    printf("\n");
  }
}

int	main(int ac, char **ag) {
  if (ac < 3) {
    dprintf(2, "%s: [X] [Y]\n", ag[0]);
    return 1;
  }
  if (checkArg(ag[1], ag[2], ag[3]) == FAILURE) {
    return FAILURE;
  }
  if (ag[3] == NULL) {
    return GeneratMatrix(atoi(ag[1]), atoi(ag[2]), 20);
  } else {
    return GeneratMatrix(atoi(ag[1]), atoi(ag[2]), atoi(ag[3]));
  }
}
