#!/usr/bin/sh

function execute() {
    echo "Matrix size: [" $1 "][" $2 "] * [" $3 "][" $4 "]" 
    echo "Naive"
    time java Naive ../MatrixFiles/$5 ../MatrixFiles/$6 > /dev/null || echo "Naive fail to multipliate matrix"
    echo "Strassen"
    time java Strassen ../MatrixFiles/$5 ../MatrixFiles/$6 > /dev/null || echo "Strassen fail to multipliate matrix"
    
}
execute 2 2 2 2 "2-1.txt" "2-2.txt"
execute 4 4 4 4 "4-1.txt" "4-2.txt"
execute 8 8 8 8 "8-1.txt" "8-2.txt"
execute 16 16 16 16 "16-1.txt" "16-2.txt"
execute 32 32 32 32 "32-1.txt" "32-2.txt"
execute 64 64 64 64 "64-1.txt" "64-2.txt"
execute 128 128 128 128 "128-1.txt" "128-2.txt"
execute 256 256 256 256 "256-1.txt" "256-2.txt"
execute 512 512 512 512 "512-1.txt" "512-2.txt"
#execute 1024 1024 1024 1024 "1024-1.txt" "1024-2.txt"
#execute 2048 2048 2048 2048 "2048-1.txt" "2048-2.txt"
